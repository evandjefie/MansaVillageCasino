﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// public class UI_Shop : MonoBehaviour
// {
//     private Transform container;
//     private Transform shopItemTemplate;
//     private IShopCustomer iShopCustomer;

//     private void Awake(){
//         container = Transform.Find("container");
//         shopItemTemplate = container.Find("shopItemTemplate");
//         shopItemTemplate.gameObject.SetActive(false);
//     }

//     private void Start()
//     {
//         CreateItemButton(Item.ItemType.HealthPotion, Item.GetSprite(Item.ItemType.HealthPotion), "HealthPotion", Item.GetCost(Item.ItemType.HealthPotion)); // Instantiate button
//     }

//     // Button Generator
//     private void CreateItemButton(Item.ItemType itemType, Sprite itemSprite, string itemName, int itemCost, int positionIndex)
//     {
//         Transform shopItemTransform = Instantiate(shopItemTemplate, container);
//         shopItemTransform.gameObject.SetActive(true);
//         RectTransform shopItemTransform = shopItemTransform.GetComponent<RectTransform>();

//         // 
//         float shopItemHeight = 90f;
//         shopItemTransform.anchoredPosition = new Vector2(0, -shopItemHeight * positionIndex);

//         // Search gameObject position and set his data
//         shopItemTransform.Find("nameText").GetComponent<TextMeshProUGUI>().SetText(itemName);
//         shopItemTransform.Find("costText").GetComponent<TextMeshProUGUI>().SetText(itemCost.ToString());

//         shopItemTransform.Find("itemImage").GetComponent<Image>().sprite = itemSprite;

//         shopItemTransform.GetComponent<Button_UI>().ClickFunc = ()=>{
//             // Clicked on shop item button
//             TryBuyItem(itemType);
//         };
//     }

//     // Buy item from button clicked
//     private void TryBuyItem(Item.ItemType itemType)
//     {
//         shopCustomer.BoughtItem(itemType);
//     }

//     public void show(IShopCustomer shopCustomer) // Methods to Show UI pop-up
//     {
//         this.shopCustomer = shopCustomer;
//         gameObject.SetActive(true);
//     }

//     public void Hide() // Methods to Hide UI pop-up
//     {
//         gameObject.SetActive(false);
//     }

// }
