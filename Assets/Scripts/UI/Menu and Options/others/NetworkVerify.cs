using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;

public class NetworkVerify : MonoBehaviour
{
    public bool clientConnected;
    public GameObject text;
    public GameObject NetworkVerif;

    bool isConnected()
    {
        if (Application.internetReachability == NetworkReachability.NotReachable) return false;
        else return true;
    }

    void Update()
    {
        clientConnected = isConnected();
        if (clientConnected)
        {
            // ConnectedText();
            NetworkVerif.SetActive(false);
        }else
        {   
            NetworkVerif.SetActive(true);
            text.GetComponent<TMPro.TextMeshProUGUI>().text = "Vérifiez votre connexion!";
        }
    }
    // public void ConnectedText(){
    //     text.GetComponent<TMPro.TextMeshProUGUI>().text = "Connecter!";
    //     yield return new WaitForSeconds(3);
    //     text.GetComponent<TMPro.TextMeshProUGUI>().text = "";
    // }
}
