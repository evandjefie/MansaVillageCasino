﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RandomHistory : MonoBehaviour
{

    public int randNum;
    public GameObject hintDisp; 
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(HintTracker());
    }

    // Update is called once per frame
    void Update() 
    {

    }

    IEnumerator HintTracker()
    {
        randNum = Random.Range(1, 4);
        if(randNum == 1)
        {
            hintDisp.GetComponent<TMPro.TextMeshProUGUI>().text = "Le coris, devise du Mansa Village";
        }else if (randNum == 2)
        {
            hintDisp.GetComponent<TMPro.TextMeshProUGUI>().text = "Affrontez d'autres joueurs et obetnez plus de pouvoir";
        }else if (randNum == 3)
        {
            hintDisp.GetComponent<TMPro.TextMeshProUGUI>().text = "Dans un monde remplis de force seul le combat nous partage";
        }
        yield return new WaitForSeconds(9);
    }
}