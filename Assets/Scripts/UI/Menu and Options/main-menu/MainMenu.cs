﻿using UnityEngine;

public class MainMenu : MonoBehaviour
{
    // public Animator TitleScreenAnimator;
    // public GameObject levelloader;

    public void Play(bool vsCPU)
    {
        // TitleScreenAnimator.SetTrigger("PlayGame");
        PlayerPrefs.SetInt("VsCPU", vsCPU ? 1 : 0);

        // Debug.Log("Main Game!");
        // SaveUserInfo();
        // levelloader.LoadNextLevel(1); // Load scene 3 where we play directly.        
    }

    public void ExitGame()
    {
        Application.Quit();
    }

 
}