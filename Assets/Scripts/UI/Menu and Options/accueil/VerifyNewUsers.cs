﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerifyNewUsers : MonoBehaviour
{
    //Object Declarations 
    public GameObject Popup_Conditions;

    private void Start()
    {
        if(PlayerPrefs.GetInt("HasLaunched", 0) == 0)//Game hasn't launched before. 0 is the default value if the player pref doesn't exist yet.
        {
            //Code to display your first time text
            Popup_Conditions.SetActive(true);
        }
        else
        {
        //Code to show the returning user's text.
            Popup_Conditions.SetActive(false);
        }
        PlayerPrefs.SetInt("HasLaunched", 1); //Set to 1, so we know the user has been here before
    }

}
