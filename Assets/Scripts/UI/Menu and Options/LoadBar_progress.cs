﻿// using UnityEngine;
// using UnityEngine.SceneManagement;
// using UnityEngine.UI;

// public class LoadBar_progress : MonoBehaviour
// {
//     public static LevelManager Instance;

//     [SerializeField] private GameObjet _loaderCanvas;
//     [SerializeField] private Image _progressBar;
    
//     // Start is called before the first frame update
//     void Awake() {
//         if (Instance == null)
//         {  
//            Instance = this;
//            DontDestroyOnLoad(gameObjet);
//         }else
//         {
//             Destroy(gameObjet);
//         }
//     }

//     public async void LoadScene(string sceneName){
//         _target = 0;
//         _progressBar.fillAmount = 0;

//         var scene = SceneManager.LoadSceneAsync(sceneName);
//         scene.allowSceneActivation = false;

//         _loaderCanvas.SetActive(true);

//         do
//         {
//             await Task.Delay(1000);
//             _target = scene.progress;
//         } while (scene.progress < 0.9f);

//         await Task.Delay(1000);

//         scene.allowSceneActivation = true;
//         _loaderCanvas.SetActive(false);

//     }

//     // Update is called once per frame
//     void Update()
//     {
//         _progressBar.fillAmount = Mathf.MoveTowards(_progressBar.fillAmount, _target, 3 * Time.deltaTime);
//     }
// }
