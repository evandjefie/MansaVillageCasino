﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SelectLevel : MonoBehaviour
{
    // Objects
    // public LevelLoader levelloader;
    
    // Declaration
    private int difficulty = 3;

    public void Debutant()
    {
        SavePlayerPrefs(0);
        //levelloader.LoadNextLevel(1);
        
    }

    public void Intermediaire()
    {
        SavePlayerPrefs(2);
        //levelloader.LoadNextLevel(1);            
    }

    public void Expert()
    {
        SavePlayerPrefs(5);
        // levelloader.LoadNextLevel(1);
    }

    private void SavePlayerPrefs(int level)
    {
        PlayerPrefs.SetInt("Difficulty", difficulty+level);  
    }    
}
