﻿using UnityEngine;
using UnityEngine.UI;

public class GameMansa : MonoBehaviour
{
	#region SIngleton:GameMansa

	public static GameMansa Instance;

	void Awake ()
	{
		if (Instance == null) {
			Instance = this;
			DontDestroyOnLoad (gameObject);
		} else {
			Destroy (gameObject);
		}
	}

	#endregion

	[SerializeField] Text[] allCoinsUIText;

	public int Coins = 25000; // mansamoney

	void Start ()
	{
		UpdateAllCoinsUIText ();
	}

	public void UseCoins (int amount)
	{
		Coins -= amount;
	}

	public bool HasEnoughCoins (int amount)
	{
		return (Coins >= amount);
	}

	public void UpdateAllCoinsUIText ()
	{
		for (int i = 0; i < allCoinsUIText.Length; i++) {
			allCoinsUIText [i].text = Coins.ToString ();
		}
	}

}