﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

public class ProfileMansa : MonoBehaviour
{
	#region Singlton:ProfileMansa

	public static ProfileMansa Instance;

	void Awake ()
	{
		if (Instance == null)
			Instance = this;
		else
			Destroy (gameObject);
	}

	#endregion
	
	//public class UserMansa
	//{
		//public string PseudoUMansa;
		//public string EmailUMansa;
		//public List<AvatarMansa> AvatarMansasList;
		//public List<ShopMansaItem> ShopMansaItemsList;	
	//}

	public class AvatarMansa
	{
		public Sprite AvatarImg;		
	}

	public List<AvatarMansa> AvatarMansasList;

	[SerializeField] GameObject AvatarMansaUITemplate;
	[SerializeField] Transform AvatarMansasScrollView;

	GameObject g;
	int newSelectedIndex, previousSelectedIndex;

	[SerializeField] Color ActiveAvatarMansaColor;
	[SerializeField] Color DefaultAvatarMansaColor;

	[SerializeField] Image CurrentAvatarMansa;

	void Start ()
	{
		GetAvailableAvatarMansa ();
		newSelectedIndex = previousSelectedIndex = 0;
	}

	void GetAvailableAvatarMansa ()
	{
		for (int i = 0; i < ShopMansa.Instance.ShopMansaItemsList.Count; i++) {
			if (ShopMansa.Instance.ShopMansaItemsList [i].IsPurchased) {
				//add all purchased AvatarMansas to AvatarMansasList
				AddAvatarMansa (ShopMansa.Instance.ShopMansaItemsList [i].ItemImage);
			}
		}

		SelectAvatarMansa (newSelectedIndex);
	}

	public void AddAvatarMansa (Sprite img)
	{
		if (AvatarMansasList == null)
			AvatarMansasList = new List<AvatarMansa> ();
		
		AvatarMansa av = new AvatarMansa (){ AvatarImg = img };
		//add av to AvatarMansasList
		AvatarMansasList.Add (av);

		//add AvatarMansa in the UI scroll view
		g = Instantiate (AvatarMansaUITemplate, AvatarMansasScrollView);
		g.transform.GetChild (0).GetComponent <Image> ().sprite = av.AvatarImg;

		//add click event
		g.transform.GetComponent <Button> ().AddEventListener (AvatarMansasList.Count - 1, OnAvatarMansaClick);
	}

	void OnAvatarMansaClick (int AvatarMansaIndex)
	{
		SelectAvatarMansa (AvatarMansaIndex);
	}

	void SelectAvatarMansa (int AvatarMansaIndex)
	{
		previousSelectedIndex = newSelectedIndex;
		newSelectedIndex = AvatarMansaIndex;
		AvatarMansasScrollView.GetChild (previousSelectedIndex).GetComponent <Image> ().color = DefaultAvatarMansaColor;
		AvatarMansasScrollView.GetChild (newSelectedIndex).GetComponent <Image> ().color = ActiveAvatarMansaColor;

		//Change AvatarMansa
		CurrentAvatarMansa.sprite = AvatarMansasList [newSelectedIndex].AvatarImg;
	}
}
