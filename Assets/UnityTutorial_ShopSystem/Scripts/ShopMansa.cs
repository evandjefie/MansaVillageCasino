﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using TMPro;

public class ShopMansa : MonoBehaviour
{
	// L'objet sera Unique
	#region Singlton:ShopMansa

	public static ShopMansa Instance;

	void Awake ()
	{
		if (Instance == null)
			Instance = this;
		else
			Destroy (gameObject);
	}

	#endregion

	[System.Serializable] public class ShopMansaItem
	{
		public string ItemName;
		public Sprite ItemImage;
		public Sprite ImgCurrency;
		public int Price;
		//public int Cauris = 1000; use for pari
		//public int MansaMoney = 1000; use for pari
		//public int Diamond = 250 = 100*MansaMoney;
		public bool IsPurchased = false;
	}

	public List<ShopMansaItem> ShopMansaItemsList;
	[SerializeField] Animator NoCoinsAnim;
 

	[SerializeField] GameObject ItemTemplate;
	GameObject g;
	[SerializeField] Transform ShopMansaScrollView;
	[SerializeField] GameObject ShopMansaPanel;
	Button buyBtn;

	void Start ()
	{
		int len = ShopMansaItemsList.Count;
		for (int i = 0; i < len; i++) {	// Showing the item list
			g = Instantiate (ItemTemplate, ShopMansaScrollView);
			g.transform.GetChild (0).GetComponent <TextMeshProUGUI>().text = ShopMansaItemsList [i].ItemName;
			g.transform.GetChild (1).GetChild (0).GetComponent <Image>().sprite = ShopMansaItemsList [i].ItemImage;
			g.transform.GetChild (2).GetChild (0).GetComponent <Image>().sprite = ShopMansaItemsList [i].ImgCurrency;
			g.transform.GetChild (2).GetChild (1).GetChild (1).GetComponent <TextMeshProUGUI>().text = ShopMansaItemsList [i].Price.ToString ();
			buyBtn = g.transform.GetChild (2).GetComponent <Button> ();
			if (ShopMansaItemsList [i].IsPurchased) {
				DisableBuyButton ();
			}
			buyBtn.AddEventListener (i, OnShopMansaItemBtnClicked);
		}
	}

	void OnShopMansaItemBtnClicked (int itemIndex)
	{
		if (GameMansa.Instance.HasEnoughCoins (ShopMansaItemsList [itemIndex].Price)) {
			GameMansa.Instance.UseCoins (ShopMansaItemsList [itemIndex].Price);
			//purchase Item
			ShopMansaItemsList [itemIndex].IsPurchased = true;

			//disable the button
			buyBtn = ShopMansaScrollView.GetChild (itemIndex).GetChild (2).GetComponent <Button> ();
			DisableBuyButton ();
			//change UI text: coins
			GameMansa.Instance.UpdateAllCoinsUIText ();

			//add avatar
			Profile.Instance.AddAvatar (ShopMansaItemsList [itemIndex].ItemImage);
		} else {
			NoCoinsAnim.SetTrigger ("NoCoins");
			Debug.Log ("Vous n'avez pas assez d'argent!!");
		}
	}

	void DisableBuyButton ()
	{
		buyBtn.interactable = false;
		buyBtn.transform.GetChild (1).GetChild (1).GetComponent <Text> ().text = "ACHETE!";
	}
	/*---------------------Open & Close ShopMansa--------------------------*/
	public void OpenShopMansa ()
	{
		ShopMansaPanel.SetActive (true);
	}

	public void CloseShopMansa ()
	{
		ShopMansaPanel.SetActive (false);
	}

}
