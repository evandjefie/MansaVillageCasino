﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Firebase;
using Firebase.Auth;
using TMPro;

public class UserDatas : MonoBehaviour
{

    // Firebase variable
    [Header("Firebase")]
    public DependencyStatus dependencyStatus;
    public FirebaseAuth auth;
    public FirebaseUser user;

    // Login Variables
    [Space]
    [Header("Login")]
    public TextMeshProUGUI useridField;
    public List<TextMeshProUGUI> usernameField;
    //public TextMeshProUGUI emailField;

    // Items Variables
    //[Space]
    //[Header("Items")]
    //public TMP_InputField nameRegisterField;
    //public TMP_InputField emailLoginField;


// BEGIN FIREBASE CONFIG

    private void Awake()
    {
        // Check that all of the necessary dependencies for firebase are present on the system
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;

            if (dependencyStatus == DependencyStatus.Available)
            {
                InitializeFirebase();
            }
            else
            {
                Debug.LogError("Could not resolve all firebase dependencies: " + dependencyStatus);
            }
        });
        
    }

    void InitializeFirebase()
    {
        //Set the default instance object
        auth = FirebaseAuth.DefaultInstance;

        auth.StateChanged += AuthStateChanged;
        AuthStateChanged(this, null);
    }

    // Track state changes of the auth object.
    void AuthStateChanged(object sender, System.EventArgs eventArgs)
    {
        if (auth.CurrentUser != user)
        {
            bool signedIn = user != auth.CurrentUser && auth.CurrentUser != null;

            if (!signedIn && user != null) // S il n est pas connecte
            {
                Debug.Log("Signed out " + user.UserId);
            }

            user = auth.CurrentUser;

            if (signedIn) // User is connected
            {
                Debug.Log("Signed in " + user.UserId);
                useridField.text = user.UserId; // Show userId
                Debug.Log("Signed in " + user.DisplayName + "\n taille input " + usernameField.Count);
                
                for (int i = 0; i < usernameField.Count; i++)
                {
                    Debug.Log("Signed in " + user.DisplayName + "\n taille input " + usernameField.Count);
                    usernameField[i].text = user.DisplayName; // Show user.displayname
                }
            }
        }
    }

// END FIREBASE CONFIG


    void Start(){

        // User data 
        for (int i = 0; i < usernameField.Count; i++)
        {
            usernameField[i].text = user.DisplayName; // Show user.displayname
        }
    }

}
