using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Splash_screen : MonoBehaviour
{
    public GameObject dark_bg;
    [SerializeField] private float timer = 2f;
    public GameObject loader;    
    public GameObject logo;    
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Pause());
    }

    // Update is called once per frame
    IEnumerator Pause()
    {
        /* switch (SceneManager.GetActiveScene().buildIndex)
         {
             case 2:
             case 3:
             default:
         }
        Debug.Log("rien!!");
        */

        if (SceneManager.GetActiveScene().buildIndex == 2)
        {
            Debug.Log("Passe!!");
            yield return new WaitForSeconds(timer-2);
            // Thibaut's code to verify if the current user is new
            logo.SetActive(true);
        }
        else {
            Debug.Log("2ardwolves!!");
            yield return new WaitForSeconds(timer);
        //Add Loading Screen (Loader)
            
            loader.SetActive(true);
            Debug.Log("Loading!!");
            yield return new WaitForSeconds(timer);

            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        }
    }
    
}
