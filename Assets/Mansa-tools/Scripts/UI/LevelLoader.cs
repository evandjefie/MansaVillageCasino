using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class LevelLoader : MonoBehaviour
{
    public Animator transition;
    public MenuAudio MenuAudio;
    public GameObject loader;
    // public Loading loading;  
    public float transitionTime = 1f;
    public int i = 1;
    [SerializeField] private float timer = 3f;

    // Update is called once per frame
    void Update()
    {
        /*
        if (Input.GetMouseButtonDown(0))
        {
            // Verifie ce sur quoi on a cliqué
            LoadNextLevel();
        }
        */
    }
    public void LoadNextLevel(int i)
    {
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex + i));
    }

    public void LoadPreviousLevel(int i)
    {
        StartCoroutine(LoadLevel(SceneManager.GetActiveScene().buildIndex - i));
    }

    IEnumerator LoadLevel(int levelIndex)
    {
        transition.SetTrigger("FadeOut");
        Debug.Log("transitionTime!!");        
        yield return new WaitForSeconds(transitionTime);
        Debug.Log("END transitionTime!!");  
        //Add Loading Screen (Loader)
        // loading.LoadGame();
        loader.SetActive(true);
        Debug.Log("Loading!!");
        yield return new WaitForSeconds(timer);
        
        if ((SceneManager.GetActiveScene().buildIndex) != 1)
        {
            FadeMenuMusic();
            Debug.Log("c est pas la 1ere scene!!");
        }
        SceneManager.LoadScene(levelIndex);
        // LoadPlayScene(levelIndex);
    }

    public void LoadPlayScene(int levelIndex)
    {
        Debug.Log("c est LoadPlayScene!!");
        // Add animation (perhaps)
        // SceneManager.LoadScene("Main", LoadSceneMode.Single);
        if ((SceneManager.GetActiveScene().buildIndex + i) == 3) // if we goes to main scene, LoadScene in single Mode
        {
            Debug.Log("c est la scene main!!");
            SceneManager.LoadScene("Main", LoadSceneMode.Single);
        }else
        {
            Debug.Log("c est la scene mainmenu!!");        
            SceneManager.LoadScene(levelIndex);
        }
    }

    public void FadeMenuMusic()
    {
        Debug.Log("c est la scene mainmenu!!");
        MenuAudio.FadeMenuMusic();
    }

/*SOUHAIT POUR OPTIMISER CODE
POUR LA TRANSITION
AJOUTER LE COMPONENT "CANVAS GROUP" A L'UI PRINCIPAL
PUIS FAIRE UN SCRIPT DANS LEQUEL VA INTERVENIR LA VARIABLE ALPHA
POUR L'APPARITION ET LA DISPARUTION DE LA SCENE ACTIVE (SceneManager.GetActiveScene())
*/
}

/*
public GameObject toggle;
public GameObject textvalue;
Verify(){
  Switch(GameObject.name)
  {
    case "Debutant":
         pari = toggle.GetToggle.IsOn();
         if(pari){
           
         }
         LoadNextLevel()
    case "Intermediaire":
         
    case "Expert":
         
  }
}

*/