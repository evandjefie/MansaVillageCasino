using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Splash_screen_back : MonoBehaviour
{
    [SerializeField] private float timer = 3f;
    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(Pause());
    }

    // Update is called once per frame
    IEnumerator Pause()
    {
        yield return new WaitForSeconds(timer);
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
    }
    
}
